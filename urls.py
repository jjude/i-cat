""" URLs for digial catalog """

from django.conf.urls.defaults import *
from cat.models import *

from django.conf import settings

#views are defined in views.py
urlpatterns = patterns('cat.views',
     #default page number is 0;
     #url like: http://cat.com/
     (r'^$', 'home'),

     #homepage with pagination enabled
     #url like: http://cat.com/p/1
     (r'^p/(?P<page_num>\d+)/$', 'home',{'template':'home.html', 'model':'b'}),


     #author with pagination enabled
     #url like: http://cat.com/a/1/ & http://cat.com/a/1/p/1
     (r'^a/(?P<objectId>\d+)/$', 'home',{'template':'author_home.html', 'model':'a'}),
     (r'^a/(?P<objectId>\d+)/p/(?P<page_num>\d+)/$', 'home',{'template':'author_home.html', 'model':'a'}),

     #display details of a book
     #url like: http://cat.com/b/1/
     (r'^b/(?P<objectId>\d+)/$', 'book_home'),

     #category with pagination enabled
     #url like: http://cat.com/c/1/ & http://cat.com/c/1/p/1
     (r'^c/(?P<objectId>\d+)/$', 'home',{'template':'cat_home.html', 'model':'c'}),
     (r'^c/(?P<objectId>\d+)/p/(?P<page_num>\d+)/$', 'home',{'template':'cat_home.html', 'model':'c'}),
     
     #populate ajax view
     (r'^populategrid/$', 'populategrid'),

#
#     #list categories
#     (r'^$','home',{'model':'c'})
#
#     #list books in a category
#     (r'^$','home',{'model':'c'})

     #help
     (r'^help/$', 'help'),

     #display form to add
     (r'^displaybookform/$','display_book_form'),
     #add a book into library
     (r'^addbook/$','addbook'),
)

#these are derived from admin
urlpatterns += patterns('',
        #admin page
        (r'^admin/', include('django.contrib.admin.urls')),

)
#serve css files for this site
if settings.DEBUG:
    urlpatterns += patterns('',
        #this is to serve css
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.SITE_MEDIA + '/site_media/'}),
        #this is to serve image files
        (r'^files/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_URL }),
    )