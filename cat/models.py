from django.db import models
from django.newforms import ModelForm
from django import newforms as forms

from django.conf import settings

# Create your models here.
class Category(models.Model):
    category = models.CharField(max_length=45)

    def __unicode__(self):
        return self.category

    def get_absolute_url(self):
        return "/c/%s" % self.id

    class Meta:
        ordering =['category']

    class Admin:
        list_display = ('category',)

class Author(models.Model):
    name = models.CharField(max_length=45)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return "/a/%s" % self.id

    class Meta:
        ordering =['name']

    class Admin:
        list_display = ('name',)


class Book(models.Model):
    isbn = models.CharField(max_length=20, blank=True, null=True)
    title = models.TextField()
    publisher = models.TextField()
    review = models.TextField()
    authors = models.ManyToManyField(Author, verbose_name="Authors")
    smallimage = models.ImageField("Small Image", upload_to=settings.MEDIA_URL, blank=True,null=True)
    mediumimage = models.ImageField("Medium Image", upload_to=settings.MEDIA_URL, blank=True,null=True)
    categories = models.ManyToManyField(Category, verbose_name="Categories")

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return "/b/%s" % self.id

    class Meta:
        ordering =['title']

    class Admin:
        list_display = ('title', 'isbn')

class AddBookForm(ModelForm):
#    isbn         = forms.CharField()
#    title        = forms.CharField()
#    publisher    = forms.CharField()
#    review       = forms.CharField()
#    smallimage   = forms.ImageField()
#    mediumimage  = forms.ImageField()
##    authors      = forms.MultipleChoiceField()
    class Meta:
        model = Book
